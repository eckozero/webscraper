#!/usr/bin/env python
from urllib2 import urlopen
from bs4 import BeautifulSoup
import html5lib

womensURL = "http://www.theperfumeshop.com/fcp/categorylist/womens/fragrances?resetFilter=true"
mensURL = "http://www.theperfumeshop.com/fcp/categorylist/mens/fragrances?resetFilter=true"

file_loc = '/home/ecko/Documents/Perfume Data/data.txt'

def get_links(soup, urlAsHTML):
	# Search for tags with "productsCont" as the id header.
	products = soup.find_all(id="productsCont")
	# Populate all instances into a list
	unfiltered_list = products[0].find_all("h4")
	filtered_list = []
	counter = 0

	while counter < len(unfiltered_list):
		# Strip out web addresses only and populate new list (list destruction != a good idea)
		filtered_list.append(unfiltered_list[counter].a["href"])
		to_write = filtered_list[counter]
		f = open(file_loc, 'a')
		# Requires " marks to feed into BeautifulSoup
		# Fails to write without utf8 encoding (also, always a safe option)
		f.write('"' + to_write.encode('utf8') + '"' + '\n')
		f.close()
		counter += 1

html = urlopen(womensURL).read()
soup = BeautifulSoup(html, "html5lib")

get_links(soup, html)

html = urlopen(mensURL).read()
soup = BeautifulSoup(html, "html5lib")

get_links(soup, html)
