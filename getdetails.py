#!/usr/bin/env python

import time
from urllib2 import urlopen
from bs4 import BeautifulSoup
import html5lib, re
# This takes ages to run. Use progress bar to stop yourself going mental
from progressbar import *

# Setup widgets for progress bar
widgets = ['Scraping: ', Percentage(), ' ', Bar(marker='=',left='[',right=']'),
           ' ']

pbar = ProgressBar(widgets=widgets, maxval=500)
pbar.start()

file_loc = '/home/ecko/Documents/Perfume Data/data.txt'
file_loc2 = '/home/ecko/Documents/Perfume Data/details.txt'

counter = 0

while counter < 500:
	f = open(file_loc, 'r')
	lines = f.readlines()
	# Open file at line "counter" to obtain individual URL
	new_html = lines[counter].rstrip()
	f.close()
	# Set html variable as "counter" line from file
	html = urlopen(new_html[1:]).read()
	soup = BeautifulSoup(html, "html5lib")
	# Needs details from productInfo and postIt div containers
	details = soup.find_all(id="productInfoCont")
	details_of_scents = soup.find_all(id="postIt_Cont")
	# Strip H1 tags for product name
	prodName = soup.h1
	f2 = open(file_loc2, 'a')
	# Remove HTML tags for easier reading
	appendString = details_of_scents[0].get_text()
	# Weird characters returned sometimes. Encode utf8 - safety first
	f2.write((prodName.get_text()).encode('utf8') + '\n')
	f2.write(appendString.encode('utf8') + '\n')
	f2.close()
	counter+=1
	# Sleep for 1 second to avoid it looking like a DDoS attack!
	time.sleep(1)
	# Update progress bar
	pbar.update(counter)
	
pbar.finish()
